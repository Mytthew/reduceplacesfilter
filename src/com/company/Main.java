package com.company;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    @FunctionalInterface
    public interface ReplacingInterface {
        IntStream replace(IntStream inputStream, IntPredicate predicate);
    }

    public static class ReducePlacesFilter implements ReplacingInterface {
        public IntStream replace(IntStream inputStream, IntPredicate predicate) {
            return inputStream.collect(ArrayList::new, (List<Integer> list, int number) -> {
                if (predicate.test(number)) {
                    list.add(number);
                }
            }, List::addAll).stream().mapToInt(Integer::intValue);
        }
    }

    public static void main(String[] args) throws IOException {
	    IntPredicate isPrime = a -> {
	        if (a == 0 || a == 1) return false;
	        for (int i = 2; i < a; i++) {
	            if (a % i == 0) {
	                return false;
                }
            }
	        return true;
        };
	    ReducePlacesFilter reducePlacesFilter = new ReducePlacesFilter();
	    List<Integer> filterResult =
                IntStream
                        .rangeClosed(1, 1000)
                        .filter(isPrime)
                        .boxed()
                        .collect(Collectors.toList());
	    List<Integer> reduceResult =
                reducePlacesFilter
                        .replace(IntStream.rangeClosed(1, 1000), isPrime)
                        .boxed()
                        .collect(Collectors.toList());
	    if (reduceResult.size() != filterResult.size()) {
            System.out.println(reduceResult.size());
            System.out.println(filterResult.size());
	        throw new IllegalStateException("Wrong implementation");
        }
	    for (int i = 0; i < reduceResult.size(); ++i) {
	        if (!reduceResult.get(i).equals(filterResult.get(i))) {
                System.out.println(reduceResult.get(i));
                System.out.println(filterResult.get(i));
	            throw new IllegalStateException("Wrong implementation");
            }
        }
    }
}
